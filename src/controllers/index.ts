export * from './ping.controller';
export * from './slide.controller';
export * from './categorie.controller';
export * from './type-categorie.controller';
export * from './temoignage.controller';
export * from './blog.controller';
