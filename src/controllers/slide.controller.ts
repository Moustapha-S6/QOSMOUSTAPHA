import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {Slide} from '../models';
import {SlideRepository} from '../repositories';

export class SlideController {
  constructor(
    @repository(SlideRepository)
    public slideRepository : SlideRepository,
  ) {}

  @post('/slides')
  @response(200, {
    description: 'Slide model instance',
    content: {'application/json': {schema: getModelSchemaRef(Slide)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Slide, {
            title: 'NewSlide',
            exclude: ['id'],
          }),
        },
      },
    })
    slide: Omit<Slide, 'id'>,
  ): Promise<Slide> {
    return this.slideRepository.create(slide);
  }

  @get('/slides/count')
  @response(200, {
    description: 'Slide model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(Slide) where?: Where<Slide>,
  ): Promise<Count> {
    return this.slideRepository.count(where);
  }

  @get('/slides')
  @response(200, {
    description: 'Array of Slide model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Slide, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Slide) filter?: Filter<Slide>,
  ): Promise<Slide[]> {
    return this.slideRepository.find(filter);
  }

  @patch('/slides')
  @response(200, {
    description: 'Slide PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Slide, {partial: true}),
        },
      },
    })
    slide: Slide,
    @param.where(Slide) where?: Where<Slide>,
  ): Promise<Count> {
    return this.slideRepository.updateAll(slide, where);
  }

  @get('/slides/{id}')
  @response(200, {
    description: 'Slide model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Slide, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(Slide, {exclude: 'where'}) filter?: FilterExcludingWhere<Slide>
  ): Promise<Slide> {
    return this.slideRepository.findById(id, filter);
  }

  @patch('/slides/{id}')
  @response(204, {
    description: 'Slide PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Slide, {partial: true}),
        },
      },
    })
    slide: Slide,
  ): Promise<void> {
    await this.slideRepository.updateById(id, slide);
  }

  @put('/slides/{id}')
  @response(204, {
    description: 'Slide PUT success',
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() slide: Slide,
  ): Promise<void> {
    await this.slideRepository.replaceById(id, slide);
  }

  @del('/slides/{id}')
  @response(204, {
    description: 'Slide DELETE success',
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.slideRepository.deleteById(id);
  }
}
