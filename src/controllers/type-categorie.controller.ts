import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {TypeCategorie} from '../models';
import {TypeCategorieRepository} from '../repositories';

export class TypeCategorieController {
  constructor(
    @repository(TypeCategorieRepository)
    public typeCategorieRepository : TypeCategorieRepository,
  ) {}

  @post('/type-categories')
  @response(200, {
    description: 'TypeCategorie model instance',
    content: {'application/json': {schema: getModelSchemaRef(TypeCategorie)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TypeCategorie, {
            title: 'NewTypeCategorie',
            exclude: ['id'],
          }),
        },
      },
    })
    typeCategorie: Omit<TypeCategorie, 'id'>,
  ): Promise<TypeCategorie> {
    return this.typeCategorieRepository.create(typeCategorie);
  }

  @get('/type-categories/count')
  @response(200, {
    description: 'TypeCategorie model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(TypeCategorie) where?: Where<TypeCategorie>,
  ): Promise<Count> {
    return this.typeCategorieRepository.count(where);
  }

  @get('/type-categories')
  @response(200, {
    description: 'Array of TypeCategorie model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(TypeCategorie, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(TypeCategorie) filter?: Filter<TypeCategorie>,
  ): Promise<TypeCategorie[]> {
    return this.typeCategorieRepository.find(filter);
  }

  @patch('/type-categories')
  @response(200, {
    description: 'TypeCategorie PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TypeCategorie, {partial: true}),
        },
      },
    })
    typeCategorie: TypeCategorie,
    @param.where(TypeCategorie) where?: Where<TypeCategorie>,
  ): Promise<Count> {
    return this.typeCategorieRepository.updateAll(typeCategorie, where);
  }

  @get('/type-categories/{id}')
  @response(200, {
    description: 'TypeCategorie model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(TypeCategorie, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(TypeCategorie, {exclude: 'where'}) filter?: FilterExcludingWhere<TypeCategorie>
  ): Promise<TypeCategorie> {
    return this.typeCategorieRepository.findById(id, filter);
  }

  @patch('/type-categories/{id}')
  @response(204, {
    description: 'TypeCategorie PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TypeCategorie, {partial: true}),
        },
      },
    })
    typeCategorie: TypeCategorie,
  ): Promise<void> {
    await this.typeCategorieRepository.updateById(id, typeCategorie);
  }

  @put('/type-categories/{id}')
  @response(204, {
    description: 'TypeCategorie PUT success',
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() typeCategorie: TypeCategorie,
  ): Promise<void> {
    await this.typeCategorieRepository.replaceById(id, typeCategorie);
  }

  @del('/type-categories/{id}')
  @response(204, {
    description: 'TypeCategorie DELETE success',
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.typeCategorieRepository.deleteById(id);
  }
}
