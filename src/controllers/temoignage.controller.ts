import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {Temoignage} from '../models';
import {TemoignageRepository} from '../repositories';

export class TemoignageController {
  constructor(
    @repository(TemoignageRepository)
    public temoignageRepository : TemoignageRepository,
  ) {}

  @post('/temoignages')
  @response(200, {
    description: 'Temoignage model instance',
    content: {'application/json': {schema: getModelSchemaRef(Temoignage)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Temoignage, {
            title: 'NewTemoignage',
            exclude: ['id'],
          }),
        },
      },
    })
    temoignage: Omit<Temoignage, 'id'>,
  ): Promise<Temoignage> {
    return this.temoignageRepository.create(temoignage);
  }

  @get('/temoignages/count')
  @response(200, {
    description: 'Temoignage model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(Temoignage) where?: Where<Temoignage>,
  ): Promise<Count> {
    return this.temoignageRepository.count(where);
  }

  @get('/temoignages')
  @response(200, {
    description: 'Array of Temoignage model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Temoignage, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Temoignage) filter?: Filter<Temoignage>,
  ): Promise<Temoignage[]> {
    return this.temoignageRepository.find(filter);
  }

  @patch('/temoignages')
  @response(200, {
    description: 'Temoignage PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Temoignage, {partial: true}),
        },
      },
    })
    temoignage: Temoignage,
    @param.where(Temoignage) where?: Where<Temoignage>,
  ): Promise<Count> {
    return this.temoignageRepository.updateAll(temoignage, where);
  }

  @get('/temoignages/{id}')
  @response(200, {
    description: 'Temoignage model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Temoignage, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(Temoignage, {exclude: 'where'}) filter?: FilterExcludingWhere<Temoignage>
  ): Promise<Temoignage> {
    return this.temoignageRepository.findById(id, filter);
  }

  @patch('/temoignages/{id}')
  @response(204, {
    description: 'Temoignage PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Temoignage, {partial: true}),
        },
      },
    })
    temoignage: Temoignage,
  ): Promise<void> {
    await this.temoignageRepository.updateById(id, temoignage);
  }

  @put('/temoignages/{id}')
  @response(204, {
    description: 'Temoignage PUT success',
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() temoignage: Temoignage,
  ): Promise<void> {
    await this.temoignageRepository.replaceById(id, temoignage);
  }

  @del('/temoignages/{id}')
  @response(204, {
    description: 'Temoignage DELETE success',
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.temoignageRepository.deleteById(id);
  }
}
