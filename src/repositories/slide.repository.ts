import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {QosDatabaseDataSource} from '../datasources';
import {Slide, SlideRelations} from '../models';

export class SlideRepository extends DefaultCrudRepository<
  Slide,
  typeof Slide.prototype.id,
  SlideRelations
> {
  constructor(
    @inject('datasources.QosDatabase') dataSource: QosDatabaseDataSource,
  ) {
    super(Slide, dataSource);
  }
}
