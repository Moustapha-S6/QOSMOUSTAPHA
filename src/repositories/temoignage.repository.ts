import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {QosDatabaseDataSource} from '../datasources';
import {Temoignage, TemoignageRelations} from '../models';

export class TemoignageRepository extends DefaultCrudRepository<
  Temoignage,
  typeof Temoignage.prototype.id,
  TemoignageRelations
> {
  constructor(
    @inject('datasources.QosDatabase') dataSource: QosDatabaseDataSource,
  ) {
    super(Temoignage, dataSource);
  }
}
