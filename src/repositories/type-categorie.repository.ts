import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {QosDatabaseDataSource} from '../datasources';
import {TypeCategorie, TypeCategorieRelations} from '../models';

export class TypeCategorieRepository extends DefaultCrudRepository<
  TypeCategorie,
  typeof TypeCategorie.prototype.id,
  TypeCategorieRelations
> {
  constructor(
    @inject('datasources.QosDatabase') dataSource: QosDatabaseDataSource,
  ) {
    super(TypeCategorie, dataSource);
  }
}
