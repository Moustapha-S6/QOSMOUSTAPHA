import {Entity, model, property} from '@loopback/repository';

@model({settings: {strict: false}})
export class Blog extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  titre_article: string;

  @property({
    type: 'number',
    required: true,
  })
  categorie_article: number;

  @property({
    type: 'string',
    required: true,
  })
  detail_article: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Blog>) {
    super(data);
  }
}

export interface BlogRelations {
  // describe navigational properties here
}

export type BlogWithRelations = Blog & BlogRelations;
