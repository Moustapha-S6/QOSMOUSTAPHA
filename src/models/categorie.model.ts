import {Entity, model, property} from '@loopback/repository';

@model({settings: {strict: false}})
export class Categorie extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  nom_categorie: string;

  @property({
    type: 'string',
    required: true,
  })
  diminutif: string;

  @property({
    type: 'string',
    required: true,
  })
  detail: string;

  @property({
    type: 'number',
    required: true,
  })
  parent: number;

  @property({
    type: 'number',
    required: true,
  })
  type_categorie: number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Categorie>) {
    super(data);
  }
}

export interface CategorieRelations {
  // describe navigational properties here
}

export type CategorieWithRelations = Categorie & CategorieRelations;
