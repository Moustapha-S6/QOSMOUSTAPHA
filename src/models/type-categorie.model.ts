import {Entity, model, property} from '@loopback/repository';

@model({settings: {strict: false}})
export class TypeCategorie extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  nom_type: string;

  @property({
    type: 'string',
    required: true,
  })
  diminutif: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<TypeCategorie>) {
    super(data);
  }
}

export interface TypeCategorieRelations {
  // describe navigational properties here
}

export type TypeCategorieWithRelations = TypeCategorie & TypeCategorieRelations;
