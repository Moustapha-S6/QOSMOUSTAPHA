import {Entity, model, property} from '@loopback/repository';

@model({settings: {strict: false}})
export class Slide extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  nom_slide: string;

  @property({
    type: 'string',
    required: true,
  })
  image: string;

  @property({
    type: 'date',
    required: true,
  })
  date: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Slide>) {
    super(data);
  }
}

export interface SlideRelations {
  // describe navigational properties here
}

export type SlideWithRelations = Slide & SlideRelations;
