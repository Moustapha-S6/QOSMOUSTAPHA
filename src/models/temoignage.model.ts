import {Entity, model, property} from '@loopback/repository';

@model({settings: {strict: false}})
export class Temoignage extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  nom_temoin: string;

  @property({
    type: 'string',
    required: true,
  })
  detail: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Temoignage>) {
    super(data);
  }
}

export interface TemoignageRelations {
  // describe navigational properties here
}

export type TemoignageWithRelations = Temoignage & TemoignageRelations;
