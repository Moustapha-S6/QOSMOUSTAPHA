import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';

const config = {
  name: 'QosDatabase',
  connector: 'mongodb',
  url: 'mongodb+srv://moustapha:OSAUJRfU8JnPdvyt@cluster0.ouk0v9w.mongodb.net/mouhqos?retryWrites=true&w=majority&appName=Cluster0'

};

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class QosDatabaseDataSource extends juggler.DataSource
  implements LifeCycleObserver {
  static dataSourceName = 'QosDatabase';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.QosDatabase', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
